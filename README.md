# ukk-restoran
Latihan UKK Kode 1 (Restoran)

# Cara Menggunakan
1. Clone projectnya ke folder htdocs (xampp).
2. Buka Command Prompt / terminal dan change directory ke project
3. Ketik "composer install" atau "composer update" di cmd / terminal (tanpa tanda ")
4. Tunggu sampai selesai
5. Buat file baru bernama .env
6. Copy isi file .env.example ke .env
7. Sesuaikan DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME dan DB_PASSWORD dengan database kalian
8. Ketik "php artisan key:generate" di cmd / terminal (tanpa tanda ")
9. Kalau sudah, ketik "php artisan migrate:fresh --seed" di cmd / terminal (tanpa tanda ")
10. Tunggu sampai selesai
11. Akses lewat url, misalnya "http://localhost/folder/ukk-restoran/home" atau akses dengan mengetikkan "php -S localhost:8000"


# Users
1. username: admin, pass: admin
2. username: waiter, pass: waiter
3. username: kasir, pass: kasir
4. username: owner, pass: owner

